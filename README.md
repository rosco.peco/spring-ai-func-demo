## Silly little Spring AI sample

This is just a sample application using Spring AI and OpenAI functions to implement
a no-context bot that can remember stuff and has access to "current" information
via functions.

The fact it doesn't keep any context is deliberate, just to test storing and retriving
stuff from an in-memory DB via functions - obviously you wouldn't normally do it this
way, there'd be a mixture of the two, and you wouldn't use SQL for the RAG... 😅

To get started, open in IntelliJ, set up a run config for `SpringAiTestApplication`
with an environment variable `SPRING_AI_OPENAI_API_KEY` containing a working OpenAI key
and then run it. It's tested here with Java 21.

You should be able to interact with it via the (awful) API:

```shell
curl -X POST http://localhost:8080/demo -H 'Content-type: application/json' --data 'What time is it in London?'
```

```
  => The current time in London is Tue Apr 23 13:14:41 BST 2024.
```

```shell
curl -X POST http://localhost:8080/demo -H 'Content-type: application/json' --data 'What is the weather like in Barcelona?'
```

```
  => The weather in Barcelona is 28°C and clear. There is a chance of meatballs later!
```

```shell
curl -X POST http://localhost:8080/demo -H 'Content-type: application/json' --data 'Remember that my favourite colour is blue'
curl -X POST http://localhost:8080/demo -H 'Content-type: application/json' --data 'Find a mountain range in the US that is named after my favourite colour, and give me the distance to it and a weather report'
```

```
  => I've remembered that your favourite colour is blue. If there's anything else you'd like me to remember, just let me know!
  => The mountain range named "Blue Ridge Mountains" is approximately 359,325,223 miles away from you. The current weather in the Blue Ridge Mountains is 28°C, clear skies, with a chance of meatballs later.
```

(Worth remembering this last one is done with no context - every request to the LLM is a fresh conversation, with
only the question asked in the prompt...)
