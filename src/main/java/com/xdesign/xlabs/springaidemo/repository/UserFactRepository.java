package com.xdesign.xlabs.springaidemo.repository;

import com.xdesign.xlabs.springaidemo.model.UserFact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserFactRepository extends JpaRepository<UserFact, Long> {
}
