package com.xdesign.xlabs.springaidemo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class WeatherService {
    public record WeatherRequest(String location) {}
    public record WeatherResponse(String location, String weather) {}

    public WeatherResponse getWeather(@NonNull final WeatherRequest request) {
        log.info("Weather service called for {}", request.location);
        return new WeatherResponse(request.location, "The weather is 28°C, Clear, chance of meatballs later");
    }
}
