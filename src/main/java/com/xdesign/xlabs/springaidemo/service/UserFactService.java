package com.xdesign.xlabs.springaidemo.service;

import com.xdesign.xlabs.springaidemo.model.UserFact;
import com.xdesign.xlabs.springaidemo.repository.UserFactRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserFactService {
    public record RetrieveUserFactRequest(String dummy) {}
    public record RetrieveUserFactResponse(List<String> facts) {}

    public record StoreUserFactRequest(String topic, String fact) {}
    public record StoreUserFactResponse(boolean stored) {}

    private final UserFactRepository repository;

    public RetrieveUserFactResponse getUserFacts(@NonNull final RetrieveUserFactRequest request) {
        log.info("Retrieving user facts");
        return new RetrieveUserFactResponse(repository.findAll().stream().map(UserFact::getFact).toList());
    }

    public StoreUserFactResponse storeUserFact(@NonNull final StoreUserFactRequest request) {
        log.info("Storing fact: {} = {}", request.topic, request.fact);
        repository.save(new UserFact(request.topic, request.fact));
        return new StoreUserFactResponse(true);
    }
}
