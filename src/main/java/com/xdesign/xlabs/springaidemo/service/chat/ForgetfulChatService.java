package com.xdesign.xlabs.springaidemo.service.chat;

import com.xdesign.xlabs.springaidemo.service.ChatService;
import lombok.RequiredArgsConstructor;
import org.springframework.ai.chat.ChatClient;
import org.springframework.ai.chat.prompt.ChatOptions;
import org.springframework.ai.chat.prompt.Prompt;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ForgetfulChatService implements ChatService {
    private final ChatClient chatClient;
    private final ChatOptions chatOptions;

    public ChatService.ChatResponse doAsking(@NonNull final ChatService.ChatRequest request) {
        return new ChatService.ChatResponse(
                null,
                chatClient.call(new Prompt(request.prompt(), chatOptions))
                    .getResult().getOutput().getContent()
        );
    }
}
