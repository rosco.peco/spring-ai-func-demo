package com.xdesign.xlabs.springaidemo.service.chat;

import com.xdesign.xlabs.springaidemo.service.ChatService;
import lombok.RequiredArgsConstructor;
import org.springframework.ai.chat.ChatClient;
import org.springframework.ai.chat.messages.Message;
import org.springframework.ai.chat.messages.UserMessage;
import org.springframework.ai.chat.prompt.ChatOptions;
import org.springframework.ai.chat.prompt.Prompt;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class InMemoryChatService implements ChatService {
    private final ChatClient chatClient;
    private final ChatOptions chatOptions;

    private final HashMap<String, List<Message>> sessionStore = new HashMap<>();

    public ChatResponse doAsking(@NonNull final ChatRequest request) {
        final List<Message> session;
        final var sessionKey = Optional.ofNullable(request.sessionId()).orElse(UUID.randomUUID().toString());

        // find (or create a new) session
        if (!sessionStore.containsKey(sessionKey)) {
            session = new ArrayList<>();
            sessionStore.put(sessionKey, session);
        } else {
            session = sessionStore.get(sessionKey);
        }

        // Add the user message to the session
        session.add(new UserMessage(request.prompt()));

        // Do the magic 🧙‍
        final var response = chatClient.call(new Prompt(session, chatOptions)).getResult().getOutput();

        // Add the assistant message to the session
        session.add(response);

        // Return the content
        return new ChatResponse(sessionKey, response.getContent());
    }
}
