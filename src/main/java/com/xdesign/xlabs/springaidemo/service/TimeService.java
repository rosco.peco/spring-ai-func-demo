package com.xdesign.xlabs.springaidemo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Date;

@Slf4j
@Service
public class TimeService {
    public record TimeRequest(String where) {}
    public record TimeResponse(String date) {}

    public TimeResponse getTime(@NonNull final TimeRequest request) {
        log.info("Getting time for {}", request.where);

        if ("NOTTINGHAM".equalsIgnoreCase(request.where)) {
            return new TimeResponse("Four minutes to midnight");
        }

        return new TimeResponse(new Date().toString());
    }
}
