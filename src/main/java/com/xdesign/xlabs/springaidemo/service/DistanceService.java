package com.xdesign.xlabs.springaidemo.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Random;

@Slf4j
@Service
@RequiredArgsConstructor
public class DistanceService {
    private final Random random;

    public record DistanceRequest(String toWhere) {}
    public record DistanceResponse(String toWhere, int distanceInMiles) {}

    public DistanceResponse getDistanceTo(@NonNull final DistanceRequest request) {
        final var result = new DistanceResponse(request.toWhere, random.nextInt());
        log.info("Distance service: responds with {}", result);
        return result;
    }
}
