package com.xdesign.xlabs.springaidemo.service;

import org.springframework.lang.NonNull;

public interface ChatService {
    record ChatRequest(String sessionId, String prompt) {}
    record ChatResponse(String sessionId, String response) {}

    ChatService.ChatResponse doAsking(@NonNull final ChatService.ChatRequest request);
}
