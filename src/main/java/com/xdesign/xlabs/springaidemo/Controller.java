package com.xdesign.xlabs.springaidemo;

import com.xdesign.xlabs.springaidemo.service.ChatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class Controller {
    private final ChatService chatService;

    public Controller(@Qualifier("forgetfulChatService") final ChatService chatService) {
        this.chatService = chatService;
    }

    @PostMapping(path="/ask")
    public ChatService.ChatResponse ask(@RequestBody final ChatService.ChatRequest request) {
        if (request != null && request.prompt() != null && !request.prompt().isEmpty()) {
            log.info("Asking: {}", request);
            return chatService.doAsking(request);
        } else {
            log.info("No question provided");
            throw new RuntimeException("No question provided");
        }
    }

    @PostMapping(path="/demo")
    public String demo(@RequestBody final String prompt) {
        return ask(new ChatService.ChatRequest(null, prompt)).response();
    }
}
