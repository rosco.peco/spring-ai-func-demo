package com.xdesign.xlabs.springaidemo.configuration;

import com.xdesign.xlabs.springaidemo.service.DistanceService;
import com.xdesign.xlabs.springaidemo.service.UserFactService;
import com.xdesign.xlabs.springaidemo.service.WeatherService;
import com.xdesign.xlabs.springaidemo.service.TimeService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;

import java.util.Random;
import java.util.function.Function;

@Configuration
public class FunctionConfig {
    @Bean
    @Description("Retrieves the current time in a given location, or at the current location if 'here' is specified")
    public Function<TimeService.TimeRequest, TimeService.TimeResponse> timeFunction(final TimeService timeService) {
        return timeService::getTime;
    }

    @Bean
    @Description("Get the weather in a given location")
    public Function<WeatherService.WeatherRequest, WeatherService.WeatherResponse> weatherFunction(final WeatherService weatherService) {
        return weatherService::getWeather;
    }

    @Bean
    @Description("Gets the distance to the thing specified in the request")
    public Function<DistanceService.DistanceRequest, DistanceService.DistanceResponse> distanceFunction(final DistanceService distanceService) {
        return distanceService::getDistanceTo;
    }

    @Bean
    @Description("Stores facts about the user so they can be remembered. Provide as much context as possible in the topic argument")
    public Function<UserFactService.StoreUserFactRequest, UserFactService.StoreUserFactResponse> storeUserFactFunction(final UserFactService userFactService) {
        return userFactService::storeUserFact;
    }

    @Bean
    @Description("Retrieves remembered facts about the user from storage")
    public Function<UserFactService.RetrieveUserFactRequest, UserFactService.RetrieveUserFactResponse> retrieveUserFactFunction(final UserFactService userFactService) {
        return userFactService::getUserFacts;
    }

    @Bean
    public Random random() {
        return new Random();
    }
}
