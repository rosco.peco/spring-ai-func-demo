package com.xdesign.xlabs.springaidemo.configuration;

import org.springframework.ai.chat.prompt.ChatOptions;
import org.springframework.ai.openai.OpenAiChatOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChatConfig {
    @Bean
    public ChatOptions chatOptions() {
        return OpenAiChatOptions.builder()
                .withFunction("timeFunction")
                .withFunction("weatherFunction")
                .withFunction("distanceFunction")
                .withFunction("storeUserFactFunction")
                .withFunction("retrieveUserFactFunction")
                .withModel("gpt-4o")
                .withTemperature(0.8f)
                .build();
    }
}
